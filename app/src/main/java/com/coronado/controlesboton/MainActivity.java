package com.coronado.controlesboton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.material.chip.Chip;

public class MainActivity extends AppCompatActivity {

    ToggleButton toggleButton;
    Chip chip;
    RadioButton radioButton1;
    Switch mSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toggleButton=findViewById(R.id.toggleButton);
        chip=findViewById(R.id.chip);

        //Otra forma de trabajar los listeners

        toggleButton.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        //Esto se ejecuta cuando hay un cambio en este control

                        if(b){
                            Toast.makeText(getApplicationContext(), "Esto está On", Toast.LENGTH_SHORT).show();
                            chip.setText("Hola");

                        }else{

                            Toast.makeText(getApplicationContext(), "Esto está off", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
        );






        //Usualmente es la forma "encontrada"
        chip.setOnClickListener(
                v->{
                    Toast.makeText(getApplicationContext(), "Hizo click en el chip", Toast.LENGTH_SHORT).show();
                }
        );

        radioButton1=findViewById(R.id.radioButton);

        radioButton1.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                        if(isChecked){
                            Toast.makeText(getApplicationContext(),"Este radio 1 está on", Toast.LENGTH_SHORT);
                            chip.setText("Radio 1 on");

                        }else{
                            chip.setText("Radio 1 off");
                        }
                    }
                }
        );



        mSwitch=findViewById(R.id.switch1);

        mSwitch.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        //Esto se ejecuta cuando hay un cambio en este control

                        if(b){
                            Toast.makeText(getApplicationContext(), "Esto está On", Toast.LENGTH_SHORT).show();
                            chip.setText("Hola");

                        }else{

                            Toast.makeText(getApplicationContext(), "Esto está off", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
        );





    }
}